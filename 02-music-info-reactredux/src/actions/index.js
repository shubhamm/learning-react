
export const changeSong = (song) => {
    return {
        type: "SONG_CHANGED",
        payload: song
    }
}