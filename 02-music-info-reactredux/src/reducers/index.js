export const songListReducer = () => {
    return [
        {
            title: "Imagine",
            length: "3:53"
        },
        {
            title: "Wonderful Tonight", 
            length: "4:01"
        },
        {
            title: "Its a wonderful world!",
            length: "2:57"
        }
    ];
}

export const songChangeReducer = (song=null, event) => {
    if( event.type === 'SONG_CHANGED') {
        return event.payload;
    }
    return song;
}