import React from "react";
import { connect } from "react-redux";

const SongInfo = props => {
    if(!props.song) {
        return <div><p>Select a song...</p></div>
    }
    return (
        <div>
            <p>
                Title:&nbsp;{props.song.title}
                <br />
                Duration:&nbsp;{props.song.length}
            </p>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        song: state.song
    };
};

export default connect(mapStateToProps)(SongInfo);
