import React from "react";
import { connect } from "react-redux";
import { changeSong } from "../actions";

const SongList = props => {
    return props.songList.map(song => {
        return (
            <div key={song.title}>
                {song.title}
                <button onClick={() => props.changeSong(song)}>Select</button>
            </div>
        );
    });
};

const mapStateToProps = state => {
    return { songList: state.songList };
};

export default connect(
    mapStateToProps,
    { changeSong }
)(SongList);
