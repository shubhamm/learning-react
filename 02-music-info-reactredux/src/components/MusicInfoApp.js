import React from "react";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";

import SongList from "./SongList";
import SongInfo from "./SongInfo";
import { songListReducer, songChangeReducer } from "../reducers";

class MusicInfoApp extends React.Component {
    render() {
        return (
            <Provider
                store={createStore(
                    combineReducers({
                        songList: songListReducer,
                        song: songChangeReducer
                    })
                )}
            >
                <div>
                    <SongList />
                    <SongInfo />
                </div>
            </Provider>
        );
    }
}

export default MusicInfoApp;
