import React from 'react';
import ReactDOM from 'react-dom';
import MusicInfoApp from  './components/MusicInfoApp';

ReactDOM.render( 
    <MusicInfoApp/>,
    document.querySelector("#root")
);