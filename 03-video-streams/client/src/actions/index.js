import {SIGN_IN, SIGN_OUT} from "./actionTypes";

export const signIn = (userInfo) => {
    return {
        type: SIGN_IN,
        payload: userInfo
    }
}

export const signOut = (userInfo) => {
    return {
        type: SIGN_OUT,
        payload: userInfo
    }
}

