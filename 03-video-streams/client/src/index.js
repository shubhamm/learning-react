import React from "react";
import ReactDOM from "react-dom";
import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import { reducer as formReducer } from 'redux-form';

import StreamClient from "./components/MainApp";
import { loginChangeReducer } from "./reducers";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

ReactDOM.render(
    <Provider
        store={createStore(
            combineReducers({
                auth: loginChangeReducer,
                form: formReducer
            }),
            composeEnhancers(applyMiddleware())
        )}
    >
        <StreamClient />
    </Provider>,
    document.querySelector("#root")
);
