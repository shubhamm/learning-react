import { SIGN_IN, SIGN_OUT } from "../actions/actionTypes";

export const loginChangeReducer = (currentState = null, event) => {
    if (event.type === SIGN_IN || event.type === SIGN_OUT) {
        return event.payload;
    }
    return currentState;
};
