import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import LoginWithGoogle from "./LoginWithGoogle";

const StreamClientHeader = props => {
    return (
        <div className="ui secondary pointing menu">
            <Link to="/" className="item">
                Streamy
            </Link>
            <div className="right menu">
                <Link to="/" className="item">{`${props.name} Streams`}</Link>
            </div>
            <LoginWithGoogle />
        </div>
    );
};

const mapStateToProps = state => {
    return {
        name:
            state.auth && state.auth.signedIn
                ? state.auth.givenName + "'s"
                : "All"
    };
};

export default connect(
    mapStateToProps,
    { form: "createStream" }
)(StreamClientHeader);