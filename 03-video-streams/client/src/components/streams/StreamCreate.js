import React from "react";
import { Field, reduxForm } from "redux-form";

class StreamCreate extends React.Component {
    renderError = ({ error, touched }) => {
        if (error && touched) {
            return (
                <div className="ui error message">
                    <div className="header">{error}</div>
                </div>
            );
        }
    };

    renderTextInput = ({ input, label, meta }) => {
        return (
            <div className="field">
                <label>{label}</label>
                <input {...input} autoComplete="off" />
                {this.renderError(meta)}
            </div>
        );
    };

    onSubmit = formValues => {
        console.log(formValues);
    };

    render() {
        return (
            <form
                className="ui form error"
                onSubmit={this.props.handleSubmit(this.onSubmit)}
            >
                <Field
                    name="title"
                    component={this.renderTextInput}
                    label="Enter Title"
                />
                <Field
                    name="description"
                    component={this.renderTextInput}
                    label="Enter Description"
                />
                <button className="ui button primary">Submit</button>
            </form>
        );
    }

    static validate(formValues = {}) {
        const errors = {};
        if (!formValues.title || formValues.title.trim().length === 0) {
            errors.title = "You must enter a title";
        }
        if (
            !formValues.description ||
            formValues.description.trim().length === 0
        ) {
            errors.description = "You must enter a description";
        }
        return errors;
    }
}

export default reduxForm({
    form: "createStream",
    validate: StreamCreate.validate
})(StreamCreate);
