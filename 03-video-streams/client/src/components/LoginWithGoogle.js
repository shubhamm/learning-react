import React from "react";
import GoogleAuth from "../utils/GoogleAuthUtil";
import { connect } from "react-redux";

import { signIn, signOut } from "../actions";

class LoginWithGoogle extends React.Component {
    state = { signedIn: null, givenName: null, userIcon: null };

    componentDidMount() {
        this.gAuth = new GoogleAuth();
        this.gAuth.addAuthChangeListener(this.onAuthChange);
    }

    onAuthChange = signedInState => {
        if (signedInState.signedIn) {
            this.setState({
                signedIn: signedInState.signedIn,
                givenName: signedInState.givenName,
                userIcon: signedInState.userIcon
            });
        } else {
            this.setState({
                signedIn: signedInState.signedIn
            });
        }
    };

    onClickSignInButton = () => {
        this.gAuth.attemptSignIn();
    };

    onClickSignOutButton = () => {
        this.gAuth.attemptSignOut();
    };

    renderAuthButton() {
        if (this.state.signedIn === null) {
            return null;
        } else if (this.state.signedIn) {
            return (
                <div>
                    <button
                        className="ui red google button"
                        onClick={this.onClickSignOutButton}
                    >
                        <i className="google icon" />
                        Sign Out
                    </button>
                </div>
            );
        } else {
            return (
                <button
                    className="ui red google button"
                    onClick={this.onClickSignInButton}
                >
                    <i className="google icon" />
                    Sign In
                </button>
            );
        }
    }

    render() {
        if (this.state.signedIn) {
            this.props.signIn(this.state);
        } else {
            this.props.signOut(this.state);
        }
        return <div>{this.renderAuthButton()}</div>;
    }
}

const mapStateToProps = state => {
    return { auth: state.auth };
};

export default connect(
    mapStateToProps,
    { signIn, signOut }
)(LoginWithGoogle);
