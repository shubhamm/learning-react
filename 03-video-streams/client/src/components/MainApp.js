import React from "react";
import { HashRouter, Route } from "react-router-dom";

import StreamClientHeader from './Header'
import StreamCreate from './streams/StreamCreate';
import StreamDelete from './streams/StreamDelete';
import StreamEdit from './streams/StreamEdit';
import StreamList from './streams/StreamList';
import StreamShow from './streams/StreamShow';

const StreamClient = () => {
    return (
        <div className="ui container">
            <HashRouter>
                <StreamClientHeader/>
                <Route path="/" exact component={StreamList} />
                <Route path="/streams/show" exact component={StreamShow} />
                <Route path="/streams/new" exact component={StreamCreate} />
                <Route path="/streams/edit" exact component={StreamEdit} />
                <Route path="/streams/delete" exact component={StreamDelete} />
            </HashRouter>
        </div>
    );
};

export default StreamClient;
