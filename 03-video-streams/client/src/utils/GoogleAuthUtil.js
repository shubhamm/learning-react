class GoogleAuth {
    authChangeListeners = [];
    constructor() {
        window.gapi.load("auth2", () => {
            window.gapi.auth2
                .init({
                    clientId:
                        "296216880554-ls03pquoo4j92slc45h6qff3cddn63ea.apps.googleusercontent.com",
                    scope: "profile email",
                    fetch_basic_profile: "true"
                })
                .then(() => {
                    this.auth = window.gapi.auth2.getAuthInstance();
                    this.invokeAuthChangeListeners(this.auth.isSignedIn.get());
                    this.auth.isSignedIn.listen(this.invokeAuthChangeListeners);
                });
        });
    }

    addAuthChangeListener = authChangeListener => {
        this.authChangeListeners.push(authChangeListener);
        this.invokeAuthChangeListener(
            authChangeListener,
            this.auth ? this.auth.isSignedIn.get() : false
        );
    };

    removeAuthChangeListener(authChangeListener) {
        this.authChangeListeners.pop(authChangeListener);
    }

    invokeAuthChangeListeners = (signedIn) => {
        this.authChangeListeners.forEach(listener => {
            this.invokeAuthChangeListener(listener, signedIn);
        });
    };

    invokeAuthChangeListener(listener, signedIn) {
        if (signedIn) {
            const basicProfile = this.auth.currentUser.get().getBasicProfile();
            listener({
                signedIn: true,
                givenName: basicProfile.getGivenName(),
                userIcon: basicProfile.getImageUrl()
            });
        } else {
            listener({
                signedIn: false
            });
        }
    }

    attemptSignIn() {
        this.auth.signIn({
            prompt: "select_account"
        });
    }

    attemptSignOut() {
        this.auth.signOut();
    }
}

export default GoogleAuth;
