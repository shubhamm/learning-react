import Axios from "axios";
import AppConfig from "../AppConfig";
import React from "react";

class ImageAPIAccessor {
    async fetchImagesForKeyword(keyword) {
        console.log("fetch called with keyword: " + keyword);

        const response = await Axios.get("https://pixabay.com/api", {
            params: {
                q: keyword,
                key: "13941044-cf2cda8761bbd3c88d8f38985",
                per_page: AppConfig.maxResults,
                page: 1,
                image_type: "photo"
            }
        });

        console.log(response);

        const images = response.data.hits.map(result => {
            return {
                imageUrl: result.webformatURL,
                id: result.id,
                alt: keyword
            };
        });
        return images;
    }

    getWatermark() {
        return (
            <span>
                Photos by <a href="https://pixabay.com">Pixabay</a>
            </span>
        );
    }
}

export default ImageAPIAccessor;
