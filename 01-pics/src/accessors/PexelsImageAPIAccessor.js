import Axios from "axios";
import AppConfig from "../AppConfig";
import React from "react";

class ImageAPIAccessor {
    async fetchImagesForKeyword(keyword) {
        const response = await Axios.get("https://api.pexels.com/v1/search", {
            params: {
                query: keyword,
                per_page: AppConfig.maxResults,
                page: 1
            },
            headers: {
                Authorization:
                    "563492ad6f917000010000017d4c60f28e9e41eeb4e5fbdf1ec3c627"
            }
        });

        console.log(response);

        const images = response.data.photos.map(result => {
            return {
                imageUrl: result.src.medium,
                id: result.id,
                alt: keyword
            };
        });
        return images;
    }

    getWatermark() {
        return (
            <span>
                Photos by <a href="https://www.pexels.com">Pexels</a>
            </span>
        );
    }
}

export default ImageAPIAccessor;
