import Axios from "axios";
import AppConfig from "../AppConfig";
import React from "react";

class ImageAPIAccessor {
    async fetchImagesForKeyword(keyword) {
        console.log("fetch called with keyword: " + keyword);

        const response = await Axios.get(
            "https://api.unsplash.com/search/photos",
            {
                params: {
                    query: keyword,
                    per_page: AppConfig.maxResults
                },
                headers: {
                    Authorization:
                        "Client-ID 679e6b07897304f912282bf17563d94f6c90b763f9ce16b7800b0268dba4332e"
                }
            }
        );

        const images = response.data.results.map(result => {
            return {
                imageUrl: result.urls.thumb,
                id: result.id,
                alt: result.alt_description
            };
        });
        return images;
    }

    getWatermark() {
        return (
            <span>
                Photos by <a href="https://unsplash.com">Unsplash</a>
            </span>
        );
    }
}

export default ImageAPIAccessor;