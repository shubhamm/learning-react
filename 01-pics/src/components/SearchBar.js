import React from 'react';

class SearchBar extends React.Component {
    
    state = {
        term: ""
    };

    onInputChange = (event) => {
        const newTerm = event.target.value;
        this.setState( 
            {
                term: newTerm
            }
        );
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit(this.state.term);
    }

    render() {
        return (
            <div className="ui segment">
                <form className="ui form" onSubmit={this.onFormSubmit}>
                    <div className="field">
                        <label htmlFor = "search-box" >Image Search</label>
                        <input type="text" id="search-box" value={this.state.term} onChange={this.onInputChange}/>
                    </div>
                </form>
            </div>
        );
    }
}

export default SearchBar;