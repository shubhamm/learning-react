import React from "react";
import SearchBar from "./SearchBar";
import ImageList from "./ImageList"
import AppConfig from "../AppConfig"

class PicturesApplication extends React.Component {

    state = {
        images: []
    }

    onSearchTermChange = async (newSearchTerm) => {
        const images = await AppConfig.imageApiAccessor.fetchImagesForKeyword(newSearchTerm);
        this.setState({images: images});
    }

    render() {
        return (
            <div className="ui container" style={{marginTop: "10px"}}>
                <SearchBar onSubmit={this.onSearchTermChange} />
                <ImageList images={this.state.images} />
            </div>
        );
    }

}

export default PicturesApplication;