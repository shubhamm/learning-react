import React from "react";
import ImageCard from "./ImageCard";
import AppConfig from "../AppConfig";

class ImageList extends React.Component {
    imageListStyle = {
        display: "grid",
        gridTemplateColumns: `repeat(auto-fill, minmax(${AppConfig.imageWidthPx}px, 1fr))`,
        gridGap: AppConfig.gridGapString,
        gridAutoRows: `${AppConfig.gridHeightPx}px`
    };

    render() {
        const imageListHtml = this.props.images.map(imageData => ( <ImageCard image={imageData}/> ));
        console.log(imageListHtml);
        return <div style={this.imageListStyle}>{imageListHtml}</div>;
    }
}

export default ImageList;
