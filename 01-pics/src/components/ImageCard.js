import React from "react";
import AppConfig from "../AppConfig";

class ImageCard extends React.Component {
    constructor(props) {
        super(props);
        this.imageReference = React.createRef();
        this.state = { spans: 0 };
    }

    adjustImageSpans = () => {
        const height = this.imageReference.current.clientHeight;
        const spans = Math.ceil(height / AppConfig.gridHeightPx) + 1;
        this.setState({ spans });
    };

    componentDidMount() {
        this.imageReference.current.addEventListener(
            "load",
            this.adjustImageSpans
        );
    }

    render() {
        return (
            <div style={{ gridRowEnd: `span ${this.state.spans}` }}>
                <img
                    style={{ width: `${AppConfig.imageWidthPx}px` }}
                    ref={this.imageReference}
                    src={this.props.image.imageUrl}
                    alt={this.props.image.alt}
                    className="ui medium rounded image"
                />
            </div>
        );
    }
}

export default ImageCard;
