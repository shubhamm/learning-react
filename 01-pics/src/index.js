import React from 'react';
import ReactDOM from 'react-dom';
import PicturesApplication from './components/PicturesApplication';

ReactDOM.render(
    <PicturesApplication/>,
    document.querySelector("#root")
);