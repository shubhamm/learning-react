import ImageAPIAccessor from "./accessors/PexelsImageAPIAccessor"

const AppConfig  = {
    imageWidthPx: 200,
    gridHeightPx: 10,
    gridGapString: "0px 5px",
    maxResults: 40,
    imageApiAccessor: new ImageAPIAccessor()
}

export default AppConfig;