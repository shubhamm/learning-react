import React from "react";
import { connect } from "react-redux";

class UserHeader extends React.Component {
  render() {
    if (this.props.user && this.props.user.name) {
      return <div className="header">{this.props.user.name}</div>;
    }
    return null;
  }
}

const mapStateToProps = (state, props) => {
  return { user: state.users[props.userId] };
};

export default connect(mapStateToProps)(UserHeader);
