import _ from "lodash";

import jsonPlaceholder from "../apis/jsonPlaceholder";
import { FETCH_POSTS, FETCH_USER } from "./actionTypes";

export const fetchPostAndUsers = () => async (dispatch,getState) => {
  await dispatch(fetchPosts());
  const userIds = _.uniq(_.map(getState().posts, "userId"));
  userIds.forEach( userId => dispatch(fetchUser(userId)));
}

export const fetchPostAndUsersAlt = () => async (dispatch,getState) => {
  await dispatch(fetchPosts());
  _.chain(getState().posts)
    .map("userId")
    .uniq()
    .forEach(userId => dispatch(fetchUser(userId)))
    .value();
}

export const fetchPosts = () => async dispatch => {
  const response = await jsonPlaceholder.get("/posts");
  dispatch({
    type: FETCH_POSTS,
    payload: response.data
  });
};

export const fetchUser = userId => async (dispatch, getState) => {
  const response = await jsonPlaceholder.get(`/users/${userId}`);
  dispatch({
    type: FETCH_USER,
    payload: response.data
  });
};
