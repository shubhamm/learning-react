import { FETCH_USER } from "../actions/actionTypes";

export const usersReducer = (state = {}, action) => {
  switch (action.type) {
    case FETCH_USER: {
      const newState = { ...state };
      if(action.payload && action.payload.id) {
        newState[action.payload.id] = action.payload;
      }
      return newState;
    }
    default:
      return state;
  }
};
