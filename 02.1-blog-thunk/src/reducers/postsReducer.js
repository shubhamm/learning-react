import { FETCH_POSTS } from "../actions/actionTypes";

export const postsReducer = (state = [], event) => {
  switch (event.type) {
    case FETCH_POSTS: {
      return event.payload;
    }
    default:
      return state;
  }
};
